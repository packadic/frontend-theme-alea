'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:ModulesNewsIndexCtrl
 * @description
 * # ModulesNewsIndexCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('ModulesNewsIndexCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
