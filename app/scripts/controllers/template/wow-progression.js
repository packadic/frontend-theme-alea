'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:TemplateWowProgressionCtrl
 * @description
 * # TemplateWowProgressionCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
    .controller('TemplateWowProgressionCtrl', function ($scope) {
        $scope.wowProgressionOptions = {
            raiders: {
                'all': ['Strawbrrykek', 'Planarity', 'Depravity', 'Coras', 'Dimnir', 'Ranged', 'Davesko', 'Lupio', 'Mistymus'],
                'mists': ['Planarity']
            }
        }
    });
