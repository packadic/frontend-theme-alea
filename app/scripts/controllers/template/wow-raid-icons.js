'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:TemplatesWowRaidIconsCtrl
 * @description
 * # TemplatesWowRaidIconsCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('TemplateWowRaidIconsCtrl', function ($scope) {
    $scope.raids = ["MC", "BWL", "AQ20", "AQ40",
        "Kara", "Gruul", "Mag", "ZA", "SSC", "TK", "MH",  "BT", "SWP",
        "Naxx", "OS", "VoA", "EoE", "Uld", "ToC", "Ony", "ICC", "RS",
        "BH", "BoT", "BWD", "ToFW", "FL", "DS",
        "MV", "HoF", "ToES", "ToT", "SoO"];
  });
