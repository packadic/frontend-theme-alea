'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:TemplateTypoCtrl
 * @description
 * # TemplateTypoCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('TemplateTypoCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
