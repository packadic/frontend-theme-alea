'use strict';

/**
 * @ngdoc function
 * @name aleaApp.controller:TemplatePortletsCtrl
 * @description
 * # TemplatePortletsCtrl
 * Controller of the aleaApp
 */
angular.module('aleaApp')
  .controller('TemplatePortletsCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
