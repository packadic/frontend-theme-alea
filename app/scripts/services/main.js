'use strict';

angular.module('aleaApp')
    .service('Main2', function Main2($http) {
        var data = false;

        this.get = function(callback){
            if(data == false){
                data = $http({method: 'GET', url: 'scripts/data/main.json'}).
                    success(function(response, status, headers, config) {
                        data = response;
                        callback(response);
                    });
            } else {
                callback(data);
            }
        };


    })
    .service('Main', function Main($http) {
        var data = false;

        this.get = function(callback){
            if(data == false){
                data = $http({method: 'GET', url: restServer.url + 'main/base'}).
                    success(function(response, status, headers, config) {
                        data = response;
                        callback(response);
                    });
            } else {
                callback(data);
            }
        };


    });
