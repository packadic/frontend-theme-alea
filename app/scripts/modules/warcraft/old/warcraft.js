"use strict"

angular.module('warcraft', ['ngCookies'])
    .factory('wowCache', ['$cacheFactory', function ($cacheFactory) {
        return $cacheFactory('wow-cache');
    }])
    .factory('WoW', ['$http', 'wowCache', function ($http, wowCache) {
        var settings = {
            region: 'eu',
            locale: 'en_GB'
        };

        var regions = {
            us: {
                region: 'US',
                host: 'us.battle.net',
                locales: [ 'en_US', 'es_MX', 'pt_BR' ]
            },
            eu: {
                region: 'Europe',
                host: 'eu.battle.net',
                locales: [ 'en_GB', 'es_ES', 'fr_FR', 'ru_RU', 'de_DE', 'pt_PT', 'it_IT' ]
            },
            kr: {
                region: 'Korea',
                host: 'kr.battle.net',
                locales: [ 'ko_KR' ]
            },
            tw: {
                region: 'Taiwan',
                host: 'tw.battle.net',
                locales: [ 'zh_TW' ]
            },
            cn: {
                region: 'China',
                host: 'www.battlenet.com.cn',
                locales: [ 'zh_CN' ]
            }
        }


        var request = function (uri, success, disableCache) {
            var url = 'http://' + regions[settings.region.toLowerCase()].host + '/api/wow/' + uri;
            var cache = wowCache.get(url);

            if (cache == undefined || disableCache == true) {
                return $http.jsonp(url + '?jsonp=JSON_CALLBACK')
                    .success(function (data, status) {
                        if (success != undefined && status == 200) {
                            wowCache.put(url, data);
                            success(data);
                        }
                    })
                    .error(function (data, status) {
                        // @todo this
                    });
            }
            else {
                return success(cache);
            }
        };


        var data = function () {
            var container = {
                battlegroups: {},
                character: {
                    races: {},
                    classes: {},
                    achievements: {}
                },
                guild: {
                    rewards: {},
                    perks: {},
                    achievements: {}
                },
                item: {
                    classes: {}
                },
                talents: {},
                pet: {
                    types: {

                    }
                }
            };

            _.each(container, function (obj, id) {
                //console.log(obj, _.size(obj), id);
                if (_.size(obj) > 0) {
                    _.each(obj, function (childObj, childId) {
                        request('data/' + id + '/' + childId, function (response) {
                            container[id][childId] = response[childId];
                        });
                    });
                }
                else {
                    request('data/' + id + (id == 'battlegroups' ? '/' : ''), function (response) {
                        container[id] = (id == 'battlegroups' ? response.battlegroups : response);
                    });
                }
            });

            return container;
        }();

        var transform = function () {
            var transformer = function (object, from, to, value) {
                var result;
                _.each(object, function (obj, i) {
                    if (obj[from] === value) {
                        result = obj[to];
                    }
                });
                return result;
            };
            var classes = function (from, to, value) {
                return transformer(data.character.classes, from, to, value);
            };
            var races = function (from, to, value) {
                return transformer(data.character.races, from, to, value);
            };
            var genders = function (from, to, value) {
                var genders = [
                    { id: 0, name: 'male' },
                    { id: 1, name: 'female' }
                ]
                return transformer(genders, from, to, value);
            };
            var itemClasses = function (from, to, value) {
                return transformer(data.item.classes, from, to, value);
            };

            return {
                gender: genders,
                itemClass: itemClasses,
                class: classes,
                race: races
            }
        }();


        var getGuildRanks = function(realm, guild, tier, size, success){
            var url = 'http://www.wowprogress.com/guild/' + settings.region + '/' + realm + '/' + guild + '/rating.tier' + tier + '_' + size + '/json_rank';
            var cache = wowCache.get(url);

            if (cache == undefined || disableCache == true) {
                $http.jsonp(url + '?&jsonp=JSON_CALLBACK', {
                    responseType: 'json', method: 'jsonp'
                })
                    .success(function (data, status) {
                        if (success != undefined && status == 200) {
                            wowCache.put(url, data);
                            success(data);
                        }
                    })
                    .error(function (data, status) {
                        // @todo this
                    });
            }
            else {
                success(cache);
            }
        }

        return {
            data: data,
            regions: regions,
            request: request,
            transform: transform,
            getGuildRanks: getGuildRanks,
            settings: settings
        }
    }]);
