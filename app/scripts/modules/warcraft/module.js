(function (window, angular, undefined) {
    'use strict';

    var WarcraftModule = angular.module('warcraft', ['ngCookies']);

    var $WarcraftService = function () {
        var _config = {

            region: 'eu',
            locale: 'en_GB',
            guild: 'Alea iacta est',
            realm: 'Hellfire',

            regions: {
                us: {
                    region: 'US',
                    host: 'us.battle.net',
                    locales: [ 'en_US', 'es_MX', 'pt_BR' ]
                },
                eu: {
                    region: 'Europe',
                    host: 'eu.battle.net',
                    locales: [ 'en_GB', 'es_ES', 'fr_FR', 'ru_RU', 'de_DE', 'pt_PT', 'it_IT' ]
                },
                kr: {
                    region: 'Korea',
                    host: 'kr.battle.net',
                    locales: [ 'ko_KR' ]
                },
                tw: {
                    region: 'Taiwan',
                    host: 'tw.battle.net',
                    locales: [ 'zh_TW' ]
                },
                cn: {
                    region: 'China',
                    host: 'www.battlenet.com.cn',
                    locales: [ 'zh_CN' ]
                }
            }
        };

        var getConfig = function () {
            return angular.copy(_config);
        };

        var setConfig = function (options) {
            angular.extend(_config, options);
        };

        return {
            config: setConfig,
            getConfig: getConfig,
            $get: ['$rootScope',
                '$location',
                '$routeParams',
                '$q',
                '$injector',
                '$http',
                '$templateCache',
                '$sce',
                function ($rootScope, $location, $routeParams, $q, $injector, $http, $templateCache, $sce) {

                    var warcraft = {
                        config: getConfig()
                    };

                    return warcraft;
                }]
        };
    };


    WarcraftModule.provider('warcraftService', $WarcraftService);

    $WarcraftCache.$inject = ['$cacheFactory'];
    function $WarcraftCache($cacheFactory) {
        return $cacheFactory('warcraft-cache');
    }

    WarcraftModule.factory('warcraftCache', $WarcraftCache);

})(window, window.angular);