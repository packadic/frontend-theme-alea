'use strict';

describe('Controller: TemplateNavCtrl', function () {

  // load the controller's module
  beforeEach(module('aleaApp'));

  var TemplateNavCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TemplateNavCtrl = $controller('TemplateNavCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
